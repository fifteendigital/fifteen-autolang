# Autolang

A package to help with managing language files as you code.

###Installation

Add Autolang to your composer.json file:

```
    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "fifteen/autolang": "1.0.*"
    },
```

Add minimum-stability: dev:
```sh
    "minimum-stability": "dev",
```

As the repository isn't on Packagist, you also need to include it in the repositories list:

```php
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-autolang.git"
        }
    ]
```

Run composer update:

```sh
> composer update
```

Register service provider in config/app.php:

```php
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        ...

        /*
         * Package Service Providers...
         */
        Fifteen\AutoLang\AutoLangServiceProvider::class,
    ];
```

Add Alang to the aliases:
```php
    'Alang'     => \Fifteen\AutoLang\Facades\AutoLang::class,
```

### Usage

Use _Alang_ where you would use _Lang_:

```php
    <p>{{ alang('app.the_record_has_been_updated') }}.</p>
```

This generates a file called app.php in the storage/lang/en folder, with an array of keys and values:

```php
<?php

return [

    'the_record_has_been_updated'  =>  "The record has been updated",

];
```

You can then edit it manually, so that it uses the new value instead.

To save the language files to the repository, they need to be published to the resources/lang folder.

You can do this by running:

```sh
$ php artisan autolang:publish app
```

This will copy the contents of storage/lang/en/app.php to resources/lang/en/app.php, and remove storage/lang/en/app.php.
 