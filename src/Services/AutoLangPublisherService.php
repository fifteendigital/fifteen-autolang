<?php 

namespace Fifteen\AutoLang\Services;

use Fifteen\AutoLang\Support\AutoLangFileManager;
use Illuminate\Support\Facades\Lang;

class AutoLangPublisherService
{

    protected $file;

    public function __construct(AutoLangFileManager $file) 
    {
        $this->file = $file;
    }

    public function run($file)
    {
        $items = $this->getItems($file);
        $this->file->publish($file, $items);
        $this->file->removeCached($file);
    }

    public function getItems($file)
    {
        // get cached file
        if (!Lang::has('Cache::' . $file)) {
            die($file . " doesn't exist.");
        }
        $cached = Lang::get('Cache::' . $file);
        
        // get existing published file
        $published = Lang::get($file);
        if (!is_array($published)) {
            $published = [];
        }

        // merge into new array and sort
        $new = array_merge($cached, $published);
        ksort($new);
        return $new;
    }

}
