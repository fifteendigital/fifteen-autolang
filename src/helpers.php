<?php

if (! function_exists('lang'))
{
    function lang($token)
    {
        extract(app('autolang')->parseToken($token));
        $string = (empty($namespace) ? '' : $namespace . '::')
            . (empty($file) ?  'app.' : $file . '.')
            . $key;
        return app('autolang')->get($string);
    }
}
