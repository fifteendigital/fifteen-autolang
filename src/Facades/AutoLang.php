<?php 

namespace Fifteen\AutoLang\Facades;

use Illuminate\Support\Facades\Facade;

class AutoLang extends Facade 
{
    protected static function getFacadeAccessor() 
    { 
        return 'autolang'; 
    }
}