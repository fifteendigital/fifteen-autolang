<?php 

namespace Fifteen\AutoLang\Support;

use Illuminate\FileSystem\FileSystem;

class AutoLangFileManager
{

    protected $file;
    protected $default_position = 36;

    public function __construct(FileSystem $file) 
    {
        $this->file = $file;
    }

    public function writeToCache($file, $items, $locale = 'en')
    {
        $path = $this->getCachePath($file, $locale);
        $this->writeToFile($path, $items);
    }

    public function publish($file, $items, $locale = 'en')
    {
        $path = $this->getLangPath($file, $locale);
        $this->writeToFile($path, $items);
    }

    public function writeToFile($path, $items)
    {   
        if (!$this->file->exists(dirname($path))) {
            $this->file->makeDirectory(dirname($path), 0777, true);
        }
        $content = $this->getNewContent($items);
        $this->file->put($path, $content);
    }

    public function getNewContent($items)
    {
        $content = '';
        foreach ($items as $key => $value) {
            $content .= "\n" . $this->getStub($key, $value);
        }
        return "<?php\n\n"
             . "return [\n"
             . $content . "\n\n"
             . "];";
    }

    public function getStub($key, $string)
    {
        $stub = "\t'{$key}'" . $this->getSpaces($key) . "=>\t\"{$string}\",";
        return $stub;
    }

    public function getSpaces($key)
    {
        $count = strlen($key);

        if ($count < $this->default_position) {
            $spaces = $this->default_position - $count;
            return str_repeat(" ", $spaces);
        } else {
            return "\t";
        }
    }

    public function removeCached($file)
    {
        $path = $this->getCachePath($file);
        $this->file->delete($path);
    }

    public function getCachePath($file, $locale = 'en')
    {
        return storage_path('lang/' . $locale . '/' . $file . '.php');
    }

    public function getLangPath($file, $locale = 'en')
    {
        return base_path('resources/lang/' . $locale . '/' . $file . '.php');
    }
}
