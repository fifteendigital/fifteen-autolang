<?php

namespace Fifteen\AutoLang;

use Fifteen\AutoLang\Support\AutoLangFileManager;
use Illuminate\Support\Facades\Lang;

class AutoLang
{

    protected $file;

    public function __construct(AutoLangFileManager $file)
    {
        $this->file = $file;
    }

    public function get($token, array $replace = [], $locale = 'en')
    {
        if (Lang::has($token)) {
            $output = Lang::get($token, $replace, $locale);
        } elseif (Lang::has('Cache::' . $this->stripNamespace($token))) {
            // use from cache
            $output = Lang::get('Cache::' . $this->stripNamespace($token), $replace, $locale);
        } else {
            // generate and save to cache
            list($file, $key) = explode('.', $this->stripNamespace($token));
            $output = $this->getOutputString($key);
            $items = $this->getCachedItems($file, $key);
            $this->file->writeToCache($file, $items, $locale);
        }
        return $output;
    }

    public function parseToken($token)
    {
        $namespacePosition = stripos($token, '::');
        if ($namespacePosition !== false) {
            $namespace = substr($token, 0, $namespacePosition);
            $token = substr($token, $namespacePosition + 2);
        } else {
            $namespace = '';
        }
        if (stripos($token, '.') !== false) {
            list($file, $key) = explode('.', $token);
        } else {
            $file = '';
            $key = $token;
        }
        return compact('namespace', 'file', 'key');
    }

    public function snippet($token, array $replace = [], $locale = 'en')
    {
        $output = $this->get($token, $replace, $locale);
        return lcfirst($output);
    }

    public function sentence($token, $suffix = '.', array $replace = [], $locale = 'en')
    {
        $output = $this->get($token, $replace, $locale);
        $output = trim(ucfirst($output), '.,?!:;/- ');
        return $output . $suffix;
    }

    public function getOutputString($string)
    {
        $string = static::labelize($string);
        return $string;
    }

    /**
    * Create human-readable text from a snake_case field name
    **/
    public static function labelize($string)
    {
        $string = str_replace(['_', '-'], [' ', ' '], $string);
        $string = ucfirst(strtolower($string));
        return $string;
    }

    public function getCachedItems($file, $key)
    {
        $items = Lang::get('Cache::' . $file);
        if (!is_array($items)) {
            $items = [];
        }
        $output = $this->getOutputString($key);
        $items[$key] = $output;
        ksort($items);
        return $items;
    }

    private function stripNamespace($token)
    {
        return (stripos($token, '::') !== false)
            ? substr($token, stripos($token, '::') + 2)
            : $token;
    }
}
