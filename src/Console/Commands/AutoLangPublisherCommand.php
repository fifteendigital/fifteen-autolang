<?php 

namespace Fifteen\AutoLang\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Fifteen\AutoLang\Services\AutoLangPublisherService as Service;

class AutoLangPublisherCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autolang:publish {filename}';
    
    protected $name = 'autolang:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish cached language files (in storage/lang) to resources/lang.';

    public function __construct(Service $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = $this->argument('filename');
        $this->service->run($file);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['filename', InputArgument::REQUIRED, "The file name (e.g. 'general')."]
        ];
    }
}
