<?php

namespace Fifteen\AutoLang;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Lang;

class AutoLangServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/helpers.php';
        }

        $this->app->singleton('autolang', function($app)
        {
            return \App::make('\Fifteen\AutoLang\AutoLang');
        });

        $this->app->singleton('autolang.publish', function($app)
        {
            return \App::make('\Fifteen\AutoLang\Console\Commands\AutoLangPublisherCommand');
        });
        $this->commands('autolang.publish');
    }

    public function boot()
    {
        Lang::addNamespace('Cache', storage_path('lang'));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['fifteen.autolang'];
    }

}
